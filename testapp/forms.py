from django import forms
from .models import UserInput
from django.forms import ModelForm

class StatusInput(forms.ModelForm):
    class Meta:
        model = UserInput
        fields = '__all__'
