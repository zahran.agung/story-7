from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import UserInput
from .forms import StatusInput

# Create your views here.

def index(request):
    if request.method == 'POST':
        form = StatusInput(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
            return redirect ("/confirm/")
    else:
        form = StatusInput()
        return render (request, 'landing.html', {'form' : form})

def confirm(request):
    post = UserInput.objects.all()
    context = {
        'Posts' : post,
    }
    return render (request, 'confirm.html', context)