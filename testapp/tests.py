from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .apps import TestappConfig
from .models import UserInput
from .forms import StatusInput
# Create your tests here.
class Testapp_UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_index_contains_greeting(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello World", response_content)

    def test_apps(self):
        self.assertEqual(TestappConfig.name, 'testapp')
        self.assertEqual(apps.get_app_config('testapp').name, 'testapp')

    def test_model_can_create_new_status(self):
        status_input = UserInput.objects.create(Status='Status')

        counting_all_status_input = UserInput.objects.all().count()
        self.assertEqual(counting_all_status_input, 1)