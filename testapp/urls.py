from django.urls import include, path
from testapp import views as profile_views

urlpatterns = [
    path('',profile_views.index , name='index'),
    path('confirm/',profile_views.confirm , name='confirm'),
]
