from django.db import models

# Create your models here.
class UserInput(models.Model):
    Name = models.CharField(max_length=50, blank = False, default = " ")
    Status = models.TextField(max_length=300, blank = False, default = " ")

    def __str__(self):
        return "{}".format(self.Name)